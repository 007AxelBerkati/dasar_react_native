import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {iBinar} from './assets';

const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <Image source={iBinar} style={styles.image}></Image>
        <View style={styles.textWrapper}>
          <Text style={styles.title}>Styling di React Native</Text>
          <Text style={styles.subTitle}>Binar - Academy - React Native</Text>
          <Text style={styles.description}>
            As a component grows in complexity, it is much cleaner and efficient
            to use StyleSheet. Create so as to define several styles in one
            place
          </Text>
          <View style={styles.btnWrapper}>
            <TouchableOpacity>
              <Text style={styles.btnText}>Understood</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.btnText}>What?!!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'yellow',
  },

  body: {
    marginTop: 30,
    marginHorizontal: 20,
    borderRadius: 15,
    elevation: 20,
    backgroundColor: 'white',
    height: '80%',
    overflow: 'hidden',
    // borderWidth: 1,
    // borderColor: 'black',
  },

  image: {
    // flex: 1,
    height: 300,
    // borderTopLeftRadius: 16,
    // borderTopRightRadius: 16,
    width: null,
  },

  textWrapper: {
    padding: 20,
  },

  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '600',
    color: 'black',
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '400',
    color: 'gray',
    paddingBottom: 10,
  },
  description: {
    fontSize: 14,
    fontWeight: '400',
    color: 'black',
    textAlign: 'justify',
  },

  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 20,
  },

  btnText: {
    color: 'green',
    fontSize: 20,
    fontWeight: '600',
  },
});
